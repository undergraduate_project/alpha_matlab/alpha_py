import alpha, json, sys, math, time


if (len(sys.argv) < 2):
    print ('No params. Exit.')
    exit(1)

PARAM = sys.argv[1]

with open(PARAM) as param_file:
   vehicle_params = alpha.VehicleParams(json.load(param_file))

vehicle = alpha.Vehicle(vehicle_params)
vehicle.drive()