#!/usr/bin/env python

#############################################################################
# Copyright 2020 ScPA StarLine Ltd. All Rights Reserved.                    #
#                                                                           #
# Created by Nikolay Dema <ndema2301@gmail.com>                             #
#                                                                           #
# Licensed under the Apache License, Version 2.0 (the "License");           #
# you may not use this file except in compliance with the License.          #
# You may obtain a copy of the License at                                   #
#                                                                           #
# http://www.apache.org/licenses/LICENSE-2.0                                #
#                                                                           #
# Unless required by applicable law or agreed to in writing, software       #
# distributed under the License is distributed on an "AS IS" BASIS,         #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  #
# See the License for the specific language governing permissions and       #
# limitations under the License.                                            #
#############################################################################

import sys
import io

from threading import Thread
from serial    import Serial, EIGHTBITS
from time      import time

from . import log
from .loops import Spinner
from .asp import *


CHECK_RATE = 2.

WRITE_RATE = 100.
READ_RATE  = 100.


class SerialVehicleInterface:

    def __init__(self, protocol, port, connect_cb    = None,
                                       disconnect_cb = None):

        self._protocol = protocol
        self._port     = port

        self._connect_cb    = connect_cb
        self._disconnect_cb = disconnect_cb

        self.connected = False

        self._serial_read_buffer = bytearray()

        self._receive_spn = None
        self._send_spn    = None

        self._init_spn = Spinner(self._init_serial, CHECK_RATE)
        self._init_spn.start()


    def _init_serial(self):

        if self._receive_spn and not self._receive_spn.wait_for_stop(1):
            return

        if self._send_spn and not self._send_spn.wait_for_stop(1):
            return

        try:
            self._serial = Serial(self._port, baudrate = 115200,
                                              bytesize = EIGHTBITS)
        except:
            return

        self._init_spn.stop()

        if self._connect_cb:
            Thread(target = self._connect_cb, daemon = True).start()

        self._receive_spn = Spinner(self._receive_by_protocol, READ_RATE)
        self._send_spn    = Spinner(self._send_by_protocol,    WRITE_RATE)

        self._receive_spn.start()
        self._send_spn.start()

        self.connected = True


    def _send_by_protocol(self):
        # TODO: max size data constraints (serial buf on receiver side has limited size)
        alpha_can_raw_data_array = self._protocol.get_raw_data_to_send()

        for raw_data in alpha_can_raw_data_array:
            raw_serial = create_raw_serial_from_raw_alpha_data(raw_data)

            try:
                self._serial.write(raw_serial)

            except:
                self._send_spn.stop()
                return


    def _receive_by_protocol(self):

        # TODO: check and clear if buffer too big
        try:
            self._serial_read_buffer += self._serial.read(self._serial.in_waiting)

        except:

            self.connected = False

            self._receive_spn.stop()                # TODO!
            self._send_spn.stop()

            try:
                self._serial.close()
            except:
                pass

            if self._disconnect_cb:
                Thread(target = self._disconnect_cb, daemon = True).start()

            self._init_spn = Spinner(self._init_serial, CHECK_RATE)
            self._init_spn.start()

            return


        read_time = time()
        buffer_len = len(self._serial_read_buffer)
        end_buffer_check_pose = buffer_len - SERIAL_PKG_LEN
        alpha_can_raw_data_array = []
        left_buffer_pose = 0

        i = 0
        while (i <= end_buffer_check_pose):
            # TODO: not optimal
            pkg = get_pkg_from_raw_serial(self._serial_read_buffer[i:i+SERIAL_PKG_LEN])

            if pkg:
                i += SERIAL_PKG_LEN
                left_buffer_pose = i
                alpha_can_raw_data_array.append(pkg)
            else:
                i += 1


        if alpha_can_raw_data_array:
            self._protocol.update_data_from_raw(alpha_can_raw_data_array, read_time)
            self._serial_read_buffer = self._serial_read_buffer[left_buffer_pose:]


    # def start_communication(self):
    #     return (self.start_sender() and self.start_receiver())
    #
    #
    # def stop_communication(self):
    #     return (self.stop_sender() and self.stop_receiver())
    #
    #
    # def start_sender(self):
    #     self._send_spn.start()
    #     return self._send_spn.is_active()
    #
    #
    # def stop_sender(self):
    #     self._send_spn.stop()
    #     return not self._send_spn.is_active()
    #
    #
    # def start_receiver(self):
    #     self._receive_spn.start()
    #     return self._receive_spn.is_active()
    #
    #
    # def stop_receiver(self):
    #     self._receive_spn.stop()
    #     return not self._receive_spn.is_active()


    def __del__(self):
        if hasattr(self, '_serial') and self._serial:
            self._serial.flush()
            self._serial.close()
